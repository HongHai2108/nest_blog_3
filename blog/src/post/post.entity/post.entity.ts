import {
    Table,
    PrimaryKey,
    AutoIncrement,
    Column,
    DataType,
    Model,
    ForeignKey,
    Length,
    BelongsTo,
} from 'sequelize-typescript';
import { User } from 'src/user/user.entity/user.entity';
import { Category } from 'src/category/category.entity/category.entity';

@Table({
    tableName: 'post',
})

export class Post extends Model<Post> {
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.BIGINT)
    id: number;

    @ForeignKey(() => User)
    @Column({
        type: DataType.UUID,
        field: 'user_id',
    })
    userId: number;

    @ForeignKey(() => Category)
    @Column({
        type: DataType.UUID,
        field: 'category_id',
    })
    categoryID: number;

    @Length({
        min: 3,
        max: 60,
    })
    @Column
    title: string;

    @Column({
        type: DataType.TEXT,
        field: 'content',
    })
    content: string;

    @BelongsTo(() => Category)
    category: Category;

    @BelongsTo(() => User)
    user: User;
 }
