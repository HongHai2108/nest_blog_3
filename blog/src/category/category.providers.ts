import { Category } from "./category.entity/category.entity";

export const categoryProviders = [{ provide: 'CategoryRepository', useValue: Category }];