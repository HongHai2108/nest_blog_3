import { ApiProperty } from '@nestjs/swagger';
import { Post } from '../post.entity/post.entity';

export class PostDto {
    @ApiProperty()
    readonly id: number;

    @ApiProperty()
    readonly authorId: number;

    @ApiProperty()
    readonly categoryID: number;

    @ApiProperty()
    readonly authorName: string;

    @ApiProperty()
    readonly title: string;

    @ApiProperty()
    readonly content: string;

    @ApiProperty()
    readonly createdAt: Date;

    @ApiProperty()
    readonly updatedAt: Date;

    constructor(post: Post) {
        this.id = post.id;
        this.authorId = post.userId;
        this.authorName = post.user.name;
        this.categoryID = post.categoryID
        this.title = post.title;
        this.content = post.content;
    }
}