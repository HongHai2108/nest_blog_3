import { User } from "./user.entity/user.entity";

export const usersProviders = [{ provide: 'UsersRepository', useValue: User }];