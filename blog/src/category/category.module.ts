import { Module } from '@nestjs/common';
import { CategoryService } from './category.service';
import { CategoryController } from './category.controller';
import { Category } from './category.entity/category.entity';
import { SequelizeModule } from '@nestjs/sequelize';
import { categoryProviders } from './category.providers';

@Module({
  imports: [SequelizeModule.forFeature([Category])],
  providers: [CategoryService, ...categoryProviders],
  controllers: [CategoryController]
})
export class CategoryModule {}
