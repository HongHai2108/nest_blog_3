import { Injectable, Inject, HttpException, HttpStatus } from '@nestjs/common';
import { Category } from './category.entity/category.entity';
import { CategoryDto } from './dto/category.dto';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';

@Injectable()
export class CategoryService {
    constructor(
        @Inject('CategoryRepository')
        private readonly categoryRepository: typeof Category,
    ) { }
    
    async findAll() {
        const categories = await this.categoryRepository.findAll<Category>();
        return categories.map(category => new CategoryDto(category))
    }

    async findOne(id: number) {
        const category = await this.categoryRepository.findByPk<Category>(id);
        if (!category) {
            throw new HttpException('No post found', HttpStatus.NOT_FOUND);
        }
        return new CategoryDto(category);
    }

    async create(createCategoryDto: CreateCategoryDto) {
        const category = new Category();
        category.name = createCategoryDto.name;
        return category.save();
    }

    async update(id: number, updateCategoryDto: UpdateCategoryDto) {
        const category = await this.categoryRepository.findByPk<Category>(id);
        category.name = updateCategoryDto.name || Category.name;
        return category.save();
    }

    async delete(id: number) {
        const category = await this.categoryRepository.findByPk<Category>(id);
        await category.destroy();
        return category;
    }
}
