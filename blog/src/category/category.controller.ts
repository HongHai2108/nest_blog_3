import {
    Controller,
    Body,
    Post,
    Get,
    Param,
    ParseIntPipe,
    Delete,
    Put,
    UseGuards
} from '@nestjs/common';
import {
    ApiCreatedResponse,
    ApiBearerAuth,
    ApiOkResponse,
    ApiParam,
} from '@nestjs/swagger';
import { CategoryService } from './category.service';
import { Category } from './category.entity/category.entity';
import { CategoryDto } from './dto/category.dto';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('category')
export class CategoryController {
    constructor(private readonly categoryService: CategoryService) { }
    
    @Get()
    @ApiOkResponse({ type: [CategoryDto] })
    findAll(): Promise<CategoryDto[]>{
        return this.categoryService.findAll();
    }

    @Get(':id')
    @ApiOkResponse({ type: CategoryDto })
    @ApiParam({ name: 'id', required: true })
    findOne(@Param('id', new ParseIntPipe()) id: number): Promise<CategoryDto>{
        return this.categoryService.findOne(id);
    }

    @Post()
    @ApiCreatedResponse({ type: Category })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    create(@Body() createCategoryDto: CreateCategoryDto): Promise<Category>{
        return this.categoryService.create(createCategoryDto)
    }

    @Put(':id')
    @ApiOkResponse({ type: Category })
    @ApiParam({ name: 'id', required: true })
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    update(
        @Param('id', new ParseIntPipe()) id: number,
        @Body() updateCategoryDto: UpdateCategoryDto
    ): Promise<Category>{
        return this.categoryService.update(id, updateCategoryDto)
    }

    @Delete(':id')
    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @ApiOkResponse({ type: Category })
    @ApiParam({ name: 'id', required: true })
    delete(@Param('id', new ParseIntPipe()) id: number): Promise<Category>{
        return this.categoryService.delete(id);
    }
}
