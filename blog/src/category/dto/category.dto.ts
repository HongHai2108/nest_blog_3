import { ApiProperty } from '@nestjs/swagger';
import { Category } from '../category.entity/category.entity';

export class CategoryDto {
    @ApiProperty()
    readonly id: number;

    @ApiProperty()
    readonly name: string;

    constructor(category: Category) {
        this.id = category.id;
        this.name = category.name;
    }
}