import {
    Table,
    Column,
    Model,
    Length,
    DataType,
    HasMany,
} from 'sequelize-typescript';
import { Post } from 'src/post/post.entity/post.entity';

@Table({
    tableName: 'category',
})

export class Category extends Model<Category> {
    @Column({
        type: DataType.UUID,
        defaultValue: DataType.UUIDV4,
        primaryKey: true,
    })
    id: number;

    @Length({
        min: 3,
        max: 60,
    })
    @Column
    name: string;

    @HasMany(() => Post)
    posts: Post[];
 }
