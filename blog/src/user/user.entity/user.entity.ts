import {
    Table,
    Column,
    Model,
    Unique,
    IsEmail,
    DataType,
    HasMany,
} from 'sequelize-typescript';
import { Post } from 'src/post/post.entity/post.entity';
import { Gender } from '../enum/gender';

@Table({
    tableName: 'user',
})

export class User extends Model<User>{
    @Column({
        type: DataType.UUID,
        defaultValue: DataType.UUIDV4,
        primaryKey: true,
    })
    id: number;

    @Unique
    @IsEmail
    @Column
    email: string;

    @Column
    password: string;

    @Column({ field: 'name' })
    name: string;

    @Column({ type: DataType.ENUM(Gender.female, Gender.male) })
    gender: Gender;

    @Column(DataType.DATEONLY)
    birthday: string;

    @HasMany(() => Post)
    posts: Post[];
 }
