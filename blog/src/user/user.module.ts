import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { JwtStrategy } from './auth/jwt-strategy';
import { SequelizeModule } from '@nestjs/sequelize';
import { User } from './user.entity/user.entity';
import { usersProviders } from './user.providers';

@Module({
  imports: [SequelizeModule.forFeature([User])],
  providers: [UserService, ...usersProviders, JwtStrategy],
  controllers: [UserController]
})
export class UserModule {}
