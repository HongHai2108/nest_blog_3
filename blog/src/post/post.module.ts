import { Module } from '@nestjs/common';
import { PostService } from './post.service';
import { PostController } from './post.controller';
import { SequelizeModule } from '@nestjs/sequelize';
import { Post } from './post.entity/post.entity';
import { postsProviders } from './post.providers';

@Module({
  imports: [SequelizeModule.forFeature([Post])],
  providers: [PostService, ...postsProviders],
  controllers: [PostController]
})
export class PostModule {}
