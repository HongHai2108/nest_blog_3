import { User } from "../user.entity/user.entity";
import { Gender } from "../enum/gender";
import { ApiProperty } from '@nestjs/swagger';

export class UserDto {
    @ApiProperty()
    id: number;

    @ApiProperty()
    readonly email: string;

    @ApiProperty()
    readonly name: string;

    @ApiProperty()
    readonly gender: Gender;

    @ApiProperty()
    readonly birthday: string;

    constructor(user: User) {
        this.id = user.id;
        this.email = user.email;
        this.name = user.name;
        this.gender = user.gender;
        this.birthday = user.birthday;
    }
}