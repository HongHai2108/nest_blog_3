import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SequelizeModule } from '@nestjs/sequelize';
import { PostModule } from './post/post.module';
import { CategoryModule } from './category/category.module';
import { UserModule } from './user/user.module';

@Module({
  imports: [
    SequelizeModule.forRoot({
      dialect: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'postgres',
      password: '21082000',
      database: 'nestjs4',
      models: [],
      autoLoadModels: true,
      synchronize: true,
    }),
    PostModule,
    CategoryModule,
    UserModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
