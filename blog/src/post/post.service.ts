import {
    Injectable,
    Inject,
    HttpException,
    HttpStatus
} from '@nestjs/common';
import { Post } from './post.entity/post.entity';
import { User } from 'src/user/user.entity/user.entity';
import { Category } from 'src/category/category.entity/category.entity';
import { PostDto } from './dto/post.dto';
import { CreatePostDto } from './dto/create-post.dto';
import { UpdatePostDto } from './dto/update-post.dto';

@Injectable()
export class PostService {
    constructor(
        @Inject('PostsRepository')
        private readonly postsRepository: typeof Post,
    ) { }

    async findAll() {
        const posts = await this.postsRepository.findAll<Post>({
            include: [User, Category],
        });
        return posts.map(post => new PostDto(post));
    }

    async findOne(id: number) {
        const post = await this.postsRepository.findByPk<Post>(id, {
            include: [User, Category],
        });
        if (!post) {
            throw new HttpException('No post found', HttpStatus.NOT_FOUND);
        }
        return new PostDto(post);
    }

    async search(title: string) {
        const post = await this.postsRepository.findByPk<Post>(title, {
            include: [User, Category],
        });
        if (!post) {
            throw new HttpException('No post found', HttpStatus.NOT_FOUND);
        }
        return new PostDto(post);
    }

    async create(userId: number, createPostDto: CreatePostDto) {
        const post = new Post();
        post.userId = userId;
        post.title = createPostDto.title;
        post.content = createPostDto.content;
        return post.save();
    }

    private async getUserPost(id: number, userId: number) {
        const post = await this.postsRepository.findByPk<Post>(id);
        if (!post) {
            throw new HttpException('No post found', HttpStatus.NOT_FOUND);
        }
        if (post.userId !== userId) {
            throw new HttpException(
                'You are unauthorized to manage this post',
                HttpStatus.UNAUTHORIZED,
            );
        }
        return post;
    }

    async update(id: number, userId: number, updatePostDto: UpdatePostDto) {
        const post = await this.getUserPost(id, userId);
        post.title = updatePostDto.title || post.title;
        post.content = updatePostDto.content || post.content;
        return post.save();
    }

    async delete(id: number, userId: number) {
        const post = await this.getUserPost(id, userId);
        await post.destroy();
        return post;
    }
}
