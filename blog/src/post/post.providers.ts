import { Post } from "./post.entity/post.entity";

export const postsProviders = [{ provide: 'PostsRepository', useValue: Post }];