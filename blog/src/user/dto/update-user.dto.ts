import {
    IsOptional,
    IsString,
    IsEnum,
    IsISO8601
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Gender } from "../enum/gender";

export class UpdateUserDto {
    @ApiProperty()
    @IsOptional()
    @IsString()
    readonly name?: string;

    @ApiProperty()
    @IsOptional()
    @IsEnum(Gender)
    readonly gender?: Gender;

    @ApiProperty()
    @IsOptional()
    @IsISO8601()
    readonly birthday?: string;
}